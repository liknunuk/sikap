var efile = $('.efile'),
    dfile = $('.dfile'),
    rfile = $('.rfile'),
    datadok = $('#dataDokumen');

    datadok.on('click','.ddtl',function(){
        let docId = this.id.split('_');
        // alert(docId[1]);
        window.location = baseurl + 'Pustaka/detail/'+docId[1];
    })

    datadok.on('click','.dchg',function(){
        let docId = this.id.split('_');
        window.location = baseurl + 'Pustaka/doctype/'+docId[1];
    })

    datadok.on('click','.drmv',function(){
        let tenan = confirm('Data berkas akan dihapus!');        
        let docId = this.id.split('_');
        if( tenan == true ){
            $.post(baseurl + 'Pustaka/pemusnahan',{
                idDokumen: docId[1]
            },function(resp){
                if(resp == "1"){
                    location.reload();
                }else{
                    alert('Data gagal dihapus');
                }
            })
        }
    })

    $('#keyword').keypress( function(e){
        if(e.keyCode == 13){
            e.preventDefault();
            let param = $('#param').val();
            let keyword = this.value;
            $.post( baseurl + 'Pustaka/cari',{
                param:param,
                keyword:keyword
            },function(resp){
                $('#dataDokumen tr').remove();
                let berkas = JSON.parse(resp);
                let control='';
                $.each( berkas , function( i , data ){
                    if( accessLevel > 1){
                        control=`<i class="fas fa-edit btn btn-primary ml-1 dchg" id="e_${data.idDokumen}"></i>
                            <i class="fas fa-tasks btn btn-success ml-1 ddtl" id="d_${data.idDokumen}"></i>
                            <i class="fas fa-trash btn btn-danger ml-1 drmv" id="r_${data.idDokumen}"></i>`;
                    }else{
                        control = '&nbsp';
                    }
                    $('#dataDokumen').append(`
                    <tr>
                        <td>${data.nomorBerkas}</td>
                        <td>${data.kategori}</td>
                        <td>${data.lokasi}</td>
                        <td>${data.tersedia}</td>
                        <td class='text-center'>
                            ${control}
                        </td>
                    </tr>
                    `)
                })
            })
        }
    })

    datadok.on('click','.dout',function(){
        let docId = this.id.split('_');
        window.location = baseurl + 'Pinjam/form/'+docId[1];
    })


