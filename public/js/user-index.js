$(document).ready(function(){
    let param, keywd;
    param = $('#param');
    keywd = $('#keyword');
    euser = $('.uchg');
    ruser = $('.urmv');
    udata = $('#userData');

    keywd.keypress( function(ev){
        if( ev.keyCode == 13){
            ev.preventDefault();
            let p = param.val(),
                k = keywd.val();
            // console.log(p,k);
            $.post(baseurl +"User/userFilter",{
                param:p,
                keyword:k
            },function(resp){
                $('#userData tr').remove();
                let users = JSON.parse(resp);
                // console.log(users);
                
                let nomor=1;
                $.each( users , function( i , data ){
                    $('#userData').append(`
                    <tr>
                        <td>${nomor++}</td>
                        <td>${data.nomorInduk}</td>
                        <td>${data.namaLengkap}</td>
                        <td>${data.nomorTelephone}</td>
                        <td>${data.accessLevel}</td>
                        <td>
                            <i class="mx-1 btn btn-info fas fa-edit uchg" id="e_${data.idPengguna}"></i>
                            <i class="mx-1 btn btn-danger fas fa-trash urmv" id="r_${data.idPengguna}"></i>
                        </td>
                    </tr>
                    `)
                })
                
            })
        }
    })

    udata.on('click','.uchg',function(){
        let userId = this.id.split('_');
        // alert(userId[1]);
        window.location = baseurl + 'User/form/'+userId[1];
    })

    udata.on('click','.urmv', function(){
        let userId = this.id.split('_');
        let tenan = confirm('Data akan dihapus');
        if( tenan == true ){
            $.post(
                baseurl+'User/fired',{
                    idPengguna:userId[1]
                },function(resp){
                    if( resp == "1" ){
                        location.reload();
                    }else{
                        alert('Data gagal dihapus');
                    }
                }
            )
        }
    })
})