<div class="row" id="brandRow">
    <div class="col-sm-1">
        <div id="brandLogo">
            <img src="https://upload.wikimedia.org/wikipedia/commons/5/51/Logo_BPN-KemenATR_%282017%29.png" alt="">
        </div>
    </div>
    <div class="col-sm-11" id="brandTitle">
        <h1><?= headerText; ?></h1>
        <h2>Badan Pertanahan Nasional Kabupaten Banjarnegara</h2>
    </div>
</div>