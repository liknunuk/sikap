<!-- Nav -->
<div class="row bg-dark">
    <div class="col">
        <nav class="navbar navbar-expand-lg navbar-dark bg-dark">
            <a class="navbar-brand" href="#"><?= webTitle; ?></a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>

            <div class="collapse navbar-collapse" id="navbarSupportedContent">
                <ul class="navbar-nav mr-auto">
                    <?php if ($_SESSION['accessLevel'] >= 1) : ?>
                        <li class="nav-item">
                            <a class="nav-link" href="<?= BASEURL . "Pustaka"; ?>">Dokumen</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="<?= BASEURL . "Laporan"; ?>">Laporan</a>
                        </li>
                    <?php endif; ?>
                    <?php if ($_SESSION['accessLevel'] >= 2) : ?>
                        <li class="nav-item">
                            <a class="nav-link" href="<?= BASEURL . "Pinjam"; ?>">Peminjaman</a>
                        </li>
                    <?php endif; ?>
                    <?php if ($_SESSION['accessLevel'] >= 3) : ?>
                        <li class="nav-item">
                            <a class="nav-link" href="<?= BASEURL . "User"; ?>">Pengguna</a>
                        </li>
                    <?php endif; ?>

                </ul>
                <ul class="navbar-nav">
                    <li class="nav-item dropdown">
                        <a class="nav-link dropdown-toggle" href="#" id="navuser" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            <?= $_SESSION['namaLengkap']; ?>
                        </a>
                        <div class="dropdown-menu" aria-labelledby="navuser">
                            <a class="dropdown-item" href="<?= BASEURL . "Home/klilan"; ?>">Logout</a>
                        </div>
                    </li>
                </ul>
            </div>
        </nav>
    </div>
</div>
<!-- Nav -->