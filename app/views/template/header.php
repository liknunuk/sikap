<!DOCTYPE html>
<html lang="en">

<head>
  <!-- Theme Made By www.w3schools.com - No Copyright -->
  <title><?= webTitle; ?> - <?= $data['title']; ?> </title>
  <meta charset="utf-8">
  <link rel="shortcut icon" href="https://nugrahamedia.web.id/nugrahamedia.png" width="16px" type="image/png">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <!-- bootstrap 4 CDN -->
  <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.0/dist/css/bootstrap.min.css" integrity="sha384-B0vP5xmATw1+K9KRQjQERJvTumQW0nPEzvF6L/Z6nronJ3oUOFUFpCjEUQouq2+l" crossorigin="anonymous">
  <!-- jquery-ui -->
  <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
  <!-- font awesome -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
  <script src="https://kit.fontawesome.com/8b0fcf1687.js" crossorigin="anonymous"></script>
  <!-- Custom style -->
  <link rel="stylesheet" href="<?= BASEURL . 'css/siputana.css'; ?>">
  <!-- Custom Script u/ restful API -->

  <script>
    var baseurl = "<?= BASEURL; ?>";
    var accessLevel = "<?= $_SESSION['accessLevel']; ?>";
  </script>
</head>

<body>
  <div class="container-fluid">