<?php
$prev = $data['pn'] > 1 ? $data['pn'] - 1 : '1';
$next = $data['pn'] + 1;
?>
<div class="pagination">
    <span class="mr-auto">
        <a href="<?= $data['pageurl'] . $prev; ?>">
            <i class="fas fa-arrow-alt-circle-left"> </i> &nbsp; Sebelum
        </a>
    </span>
    <span>
        <a href="<?= $data['pageurl'] . $next; ?>">
            Berikut &nbsp;<i class="fas fa-arrow-alt-circle-right"> </i>
        </a>
    </span>
</div>