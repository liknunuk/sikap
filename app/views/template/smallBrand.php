<div class="row" id="brandRowSmall">
    <div class="col-sm-1 krem">
        <div id="brandLogoSmall">
            <img src="https://upload.wikimedia.org/wikipedia/commons/5/51/Logo_BPN-KemenATR_%282017%29.png" alt="">
        </div>
    </div>
    <div class="col-sm-11" id="brandTitle">
        <h5>
            <?= headerText; ?><br />Badan Pertanahan Nasional Kabupaten Banjarnegara
        </h5>
    </div>
</div>