<div class="row boxTitle">
    <div class="col">
        <h4>Form Peminjaman Dokumen Pertanahan</h4>
    </div>
</div>
<div class="row mt-3">
    <div class="col-md-6 mx-auto wh90">
        <!-- trxPinjam,peminjam,petugas,idDokumen,tgPinjam -->
        <form action="<?= BASEURL . "Pinjam/set"; ?>" method="post">
            <input type="hidden" name="petugas" value="<?= $_SESSION['idPengguna']; ?>">
            <div class="form-group row">
                <label for="trxPinjam" class="col-md-4">Nomor Peminjaman</label>
                <div class="col-md-8">
                    <input type="text" name="trxPinjam" id="trxPinjam" class="form-control" readonly value="<?= $data['trxId']; ?>">
                </div>
            </div>

            <div class="form-group row">
                <label for="tgPinjam" class="col-md-4">Tanggal Pinjam</label>
                <div class="col-md-8">
                    <input type="date" class="form-control" id="tgPinjam" name="tgPinjam" value="<?= date('Y-m-d'); ?>">
                </div>
            </div>

            <div class="form-group row">
                <label for="peminjam" class="col-md-4">Nama Peminjam</label>
                <div class="col-md-8">
                    <input type="text" class="form-control" id="peminjam" name="peminjam" list="borrower">
                    <datalist id="borrower"></datalist>
                </div>
            </div>

            <div class="form-group row">
                <label for="scanDocId" class="col-md-4">ID Dokumen</label>
                <div class="col-md-8">
                    <video id="preview" style="width:450px;"></video>
                    <!-- <input type="text" class="form-control" id="scanDocId" placeholder="Scan Barcode Dokumen"> -->
                    <textarea name="scanDocId" id="scanDocId" rows="10" class="form-control" style="font-family:monospace;"></textarea>
                </div>
            </div>

            <div class="form-group row">
                <div class="col d-flex justify-content-center">
                    <button type="submit" class="btn btn-primary">Simpan</button>
                </div>

            </div>

        </form>
    </div>
</div>

<?php $this->view('template/bs4js'); ?>
<script src="<?= BASEURL . "js/pinjam-form.js"; ?>"></script>
<script src="https://rawgit.com/schmich/instascan-builds/master/instascan.min.js"></script>

<script type="text/javascript">
    var scanner = new Instascan.Scanner({
        // Whether to scan continuously for QR codes. If false, use scanner.scan() to manually scan.
        // If true, the scanner emits the "scan" event when a QR code is scanned. Default true.
        continuous: true,

        // The HTML element to use for the camera's video preview. Must be a <video> element.
        // When the camera is active, this element will have the "active" CSS class, otherwise,
        // it will have the "inactive" class. By default, an invisible element will be created to
        // host the video.
        video: document.getElementById('preview'),

        // Whether to horizontally mirror the video preview. This is helpful when trying to
        // scan a QR code with a user-facing camera. Default true.
        mirror: false,

        // Whether to include the scanned image data as part of the scan result. See the "scan" event
        // for image format details. Default false.
        captureImage: false,

        // Only applies to continuous mode. Whether to actively scan when the tab is not active.
        // When false, this reduces CPU usage when the tab is not active. Default true.
        backgroundScan: true,

        // Only applies to continuous mode. The period, in milliseconds, before the same QR code
        // will be recognized in succession. Default 5000 (5 seconds).
        refractoryPeriod: 5000,

        // Only applies to continuous mode. The period, in rendered frames, between scans. A lower scan period
        // increases CPU usage but makes scan response faster. Default 1 (i.e. analyze every frame).
        scanPeriod: 1
        // video: document.getElementById('preview'),
        // scanPeriod: 5,
        // mirror: false
    });
    scanner.addListener('scan', function(content) {
        // alert(content);
        // $('#scanDocId').val(content);
        setDocumentsId(content);
        //window.location.href=content;
    });
    Instascan.Camera.getCameras().then(function(cameras) {
        console.log(cameras);
        if (cameras.length > 0) {
            scanner.start(cameras[0]);
            $('[name="options"]').on('change', function() {
                if ($(this).val() == 1) {
                    if (cameras[0] != "") {
                        scanner.start(cameras[0]);
                    } else {
                        alert('No Front camera found!');
                    }
                } else if ($(this).val() == 2) {
                    if (cameras[1] != "") {
                        scanner.start(cameras[1]);
                    } else {
                        alert('No Back camera found!');
                    }
                }
            });
        } else {
            console.error('No cameras found.');
            alert('No cameras found.');
        }
    }).catch(function(e) {
        console.error(e);
        alert(e);
    });

    function setDocumentsId(docId) {
        let docs1 = $('#scanDocId').val();
        let docs2 = docs1 + docId + ", \n";
        $('#scanDocId').val(docs2);
    }
</script>