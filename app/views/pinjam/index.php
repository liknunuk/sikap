<div class="row boxTitle">
    <div class="col">
        <h4>Peminjaman Dokumen Pertanahan</h4>
    </div>
</div>

<div class="row mt-3">
    <div class="col wh90 table-responsive">
        <div class="d-flex justify-content-end pr-1">
            <a href="<?= BASEURL . "Pinjam/form"; ?>" class="btn btn-primary">
                <i class="fas fa-plus"></i>&nbsp; Peminjaman
            </a>
        </div>
        <?php Alert::sankil(); ?>
        <table class="table table-sm table-striped">
            <thead>
                <tr>
                    <th>No. Peminjaman</th>
                    <th>Tanggal Pinjam</th>
                    <th>Tanggal Kembali</th>
                    <th>Peminjam</th>
                    <th>Petugas</th>
                    <th class="text-center"><i class="fas fa-gear"></i></th>
                </tr>
            </thead>
            <tbody id="dataPinjam">

                <?php foreach ($data['peminjaman'] as $pinjam) : ?>
                    <tr>
                        <td><?= $pinjam['trxPinjam']; ?></td>
                        <td><?= $pinjam['tgPinjam']; ?></td>
                        <td><?= $pinjam['tgMbalik']; ?></td>
                        <td><?= $pinjam['namaPeminjam']; ?></td>
                        <td><?= $pinjam['namaPetugas']; ?></td>
                        <td class='text-center'>
                            <a href="<?= BASEURL . "Pinjam/detil/{$pinjam['trxPinjam']}"; ?>" class="btn btn-primary"><i class="fas fa-tasks"></i></a>
                            <!-- <i class="fas fa-undo btn btn-info balikberkas" id="<?= $pinjam['idDokumen']; ?>"></i> -->
                            <!-- <i class="fas fa-undo btn btn-success balikpinjam" id="<?= $pinjam['trxPinjam']; ?>"></i> -->
                        </td>
                    </tr>
                <?php endforeach; ?>

            </tbody>
        </table>
    </div>
</div>
<?php $this->view('template/bs4js'); ?>
<script src="<?= BASEURL . 'js/pinjam-index.js' ?>"></script>