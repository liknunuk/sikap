<div class="row boxTitle">
    <div class="col">
        <h4>DATA PEMINJAMAN NOMOR <?= $data['trxPinjam']; ?></h4>
    </div>
</div>

<div class="row mt-3">
    <div class="col wh90 table-responsive">
        <table class="table table-sm table-striped">
            <thead>
                <tr>
                    <th class="align-middle">Nomor Pinjam</th>
                    <th class="align-middle">Nomor Berkas<br />Kategory</th>
                    <th class="align-middle">Tanggal Pinjam</th>
                    <th class="align-middle">Peninjam</th>
                    <th class="align-middle">Petugas</th>
                    <th class="align-middle">Status Berkas</th>
                    <th class="align-middle"><i class="fas fa-gear"></i></th>
                </tr>
            </thead>
            <tbody>
                <?php foreach ($data['pinjam'] as $pinjam) : ?>
                    <tr>
                        <td><?= $pinjam['trxPinjam']; ?></td>
                        <td><?= $pinjam['nomorBerkas']; ?><br /><?= $pinjam['kategori']; ?></td>
                        <td><?= $pinjam['tgPinjam']; ?></td>
                        <td><?= $pinjam['peminjam']; ?></td>
                        <td><?= $pinjam['petugas']; ?></td>
                        <td><?= $pinjam['tersedia'] == "Ada" ? "Sudah kembali" : "Belum kembali"; ?></td>
                        <td>
                            <i class="fas fa-undo btn btn-info balikberkas" id="<?= $pinjam['idDokumen']; ?>"></i>
                        </td>
                    </tr>
                <?php endforeach; ?>
                <tr>
                    <td colspan='7' class='text-center'>
                        <a href="javascript:void(0)" class="btn btn-success balikpinjam" id="<?= $data['trxPinjam']; ?>">Kembali Semua</a>
                    </td>
                </tr>
            </tbody>
        </table>
    </div>
</div>

<?php $this->view('template/bs4js'); ?>
<script src="<?= BASEURL . 'js/pinjam-index.js' ?>"></script>