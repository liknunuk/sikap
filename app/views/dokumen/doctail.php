<div class="row boxTitle">
    <div class="col">
        <h4>Detail Berkas <?= $data['berkas']['nomorBerkas']; ?></h4>
    </div>
</div>
<div class="row mt-3">
    <div class="col-md-6 mx-auto wh90">
        <table class="table table-bordered table-sm">
            <tbody>
                <tr>
                    <td class="text-center bg-secondary" colspan="2">Informasi Umum</td>
                </tr>
                <tr>
                    <th>Nomor Berkas</th>
                    <td><?= $data['berkas']['nomorBerkas']; ?></td>
                </tr>
                <tr>
                    <th>Kategori</th>
                    <td><?= $data['berkas']['kategori']; ?></td>
                </tr>
                <tr>
                    <th>Lokasi Berkas</th>
                    <td>
                        <?php
                        list($rg, $lm, $br, $or) = explode(':', $data['berkas']['lokasi']);
                        echo 'Ruang : ' . $rg . ' Lemari : ' . $lm . ' Baris : ' . $br . ' Deret : ' . $or;
                        ?>
                    </td>
                </tr>
                <tr>
                    <th>Ketersediaan</th>
                    <td><?= $data['berkas']['tersedia']; ?></td>
                </tr>
                <tr>
                    <td class="text-center bg-secondary" colspan="2">Informasi lain</td>
                </tr>
                <?php
                $properties = json_decode($data['berkas']['property']);
                foreach ($properties as $par => $val) :
                ?>
                    <tr>
                        <th><?= $data['properties'][$par]; ?></th>
                        <td><?= $val; ?></td>
                    </tr>
                <?php endforeach; ?>
            </tbody>
        </table>
        <div class="text-center">
            <a href="<?= BASEURL . "Pustaka"; ?>" class="btn btn-success">
                <i class="fas fa-arrow-left"></i> Kembali
            </a>
        </div>

    </div>
</div>
<!--   [idDokumen] => cf0f06b249ad3d22d4b38109c6f8cbbf
    [nomorBerkas] => 1112-212
    [kategori] => Surat Ukur
    [lokasi] => 01:02:03:21
    [tersedia] => Ada
    [property] => {"nub":"1112","nib":"1112","tahun":"2021","pemohon":"Guntoro","desa":"Kecepit","kecamatan":"Banjarnegara"} -->