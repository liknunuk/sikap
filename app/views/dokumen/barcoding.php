<!DOCTYPE html>
<html>

<head>
    <title><?= $data['title']; ?></title>
    <meta charset="utf-8">
    <link rel="shortcut icon" href="https://nugrahamedia.web.id/nugrahamedia.png" width="16px" type="image/png">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- bootstrap 4 CDN -->
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.0/dist/css/bootstrap.min.css" integrity="sha384-B0vP5xmATw1+K9KRQjQERJvTumQW0nPEzvF6L/Z6nronJ3oUOFUFpCjEUQouq2+l" crossorigin="anonymous">
    <style>
        @media print {
            .raketok {
                display: none;
            }
        }
    </style>
</head>

<body onload=window.print()>
    <div class="container">
        <div class="row mt-3">
            <div class="col-lg-3 mx-auto">
                <div class="text-center" id="qrCodeCanvas"></div>
                <div class="text-center" style="font-family:monospace; font-size:10pt;"><?= $data['docId']; ?></div>
                <div class="raketok text-center"><a href="<?= BASEURL; ?>Pustaka" class="btn btn-primary">Kembali</a></div>
            </div>

        </div>
    </div>
    <?php $this->view('template/bs4js'); ?>
    <script type="text/javascript" src="<?= BASEURL; ?>js/jquery.qrcode.min.js"></script>
    <script type="text/javascript" src="<?= BASEURL; ?>js/qrcode.js"></script>
    <script>
        jQuery('#qrCodeCanvas').qrcode({
            text: "<?= $data['docId']; ?>",
        });
    </script>



</body>

</html>