<div class="row boxTitle">
    <div class="col">
        <h4>Blanko <?= $data['doctitle']; ?></h4>
    </div>
</div>
<div class="row mt-3">
    <div class="col-lg-6 mx-auto wh90">
        <form action="<?= BASEURL . "Pustaka/setArsip"; ?>" method="post">
            <input type="hidden" name="modus" value="<?= $data['modus']; ?>">
            <!-- idDokumen , kategori , lokasi , tersedia , property -->
            <div class="form-group row">
                <label for="nomorBerkas" class="col-md-4">Nomor Berkas</label>
                <div class="col-md-8">
                    <input type="text" name="nomorBerkas" id="nomorBerkas" class="form-control" value="<?php echo $data['modus'] == 'append' ? NULL : $data['berkas']['nomorBerkas']; ?>" required>
                </div>
            </div>

            <div class="form-group row">
                <label for="kategori" class="col-md-4">Kategori</label>
                <div class="col-md-8">
                    <input type="text" name="kategori" id="kategori" class="form-control" value="<?php echo $data['doctitle']; ?>" readonly>
                </div>
            </div>

            <div class="form-group row">
                <label for="lokasi" class="col-md-4">Lokasi Penyimpanan</label>
                <div class="col-md-8">
                    <input type="text" name="lokasi" id="lokasi" class="form-control" value="<?php echo $data['modus'] == 'append' ? NULL : $data['berkas']['lokasi']; ?>" placeholder="RR:LL:BB:00" pattern="[0-9][0-9]:[0-9][0-9]:[0-9][0-9]:[0-9][0-9]" required>
                </div>
            </div>

            <div class="form-group row">
                <label for="tersedia" class="col-md-4">Ketersediaan</label>
                <div class="col-md-8">
                    <select name="tersedia" id="tersedia" class="form-control">
                        <option value="Ada">Ada</option>
                        <option value="Dipinjam">Dipinjam</option>
                    </select>
                </div>
            </div>

            <?php
            $property = json_decode($data['berkas']['property'], 1);
            foreach ($data['properties'] as $k => $q) :
            ?>
                <div class="form-group row">
                    <label for="<?= $k; ?>" class="col-md-4"><?= $q; ?></label>
                    <div class="col-md-8">
                        <input type="text" name="property[<?= $k; ?>]" id="<?= $k; ?>" class="form-control" value="<?php echo $data['modus'] == 'append' ? NULL : $property[$k]; ?>">
                    </div>
                </div>
            <?php endforeach; ?>

            <div class="form-group d-flex justify-content-center">
                <button type="submit" class="btn btn-primary">Simpan</button>
            </div>
        </form>
        <!-- idPengguna , namaLengkap , nomorInduk , nomorTelephone , username , userauth , accessLevel -->
    </div>
</div>

<?php $this->view('template/bsjs'); ?>