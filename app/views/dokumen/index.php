<div class="row boxTitle">
    <div class="col">
        <h4>Arsip Dokumen Pertanahan</h4>
    </div>
</div>
<div class="row mt-3">
    <div class="col-lg-6 form-inline">
        <select id="param" class="form-control mr-1">
            <option value="">Pilih filter pencarian</option>
            <option value="idDokumen">Nomor Dokumen</option>
            <option value="kategori">Kategori</option>
            <option value="property">Keterangan Lain</option>
        </select>
        <input type="text" id="keyword" class="form-control mr-1" placeholder="Kata Kunci Pencarian">
    </div>

    <div class="col-lg-6 d-flex justify-content-end">
        <?php if ($_SESSION['accessLevel'] >= 2) : ?>
            <a href="<?= BASEURL . "Pustaka/form/bt"; ?>" class="btn btn-success ml-1">
                <i class="fas fa-plus"></i> Buku Tanah
            </a>
            <a href="<?= BASEURL . "Pustaka/form/su"; ?>" class="btn btn-info ml-1">
                <i class="fas fa-plus"></i> Surat Ukur
            </a>
            <a href="<?= BASEURL . "Pustaka/form/gu"; ?>" class="btn btn-primary ml-1">
                <i class="fas fa-plus"></i> Gambar Ukur
            </a>
        <?php endif; ?>
    </div>
</div>
<div class="row mt-3">
    <div class="col wh90 table-responsive">
        <!-- idDokumen , kategori , lokasi , tersedia , property -->
        <?php Alert::sankil(); ?>
        <table class="table table-sm table-striped">
            <thead>
                <tr>
                    <th>No. Dokumen</th>
                    <th>Kategori</th>
                    <th>Lokasi</th>
                    <th>Ketersediaan</th>
                    <th><i class="fas fa-gear"></i></th>
                </tr>
            </thead>
            <tbody id="dataDokumen">
                <?php foreach ($data['berkas'] as $berkas) : ?>
                    <tr>
                        <td><?= $berkas['nomorBerkas']; ?></td>
                        <td><?= $berkas['kategori']; ?></td>
                        <td><?= $berkas['lokasi']; ?></td>
                        <td><?= $berkas['tersedia']; ?></td>
                        <td class='text-center'>
                            <?php if ($_SESSION['accessLevel'] > 1) : ?>
                                <i class="fas fa-edit btn btn-primary ml-1 dchg" id="e_<?= $berkas['idDokumen']; ?>"></i>
                                <i class="fas fa-tasks btn btn-success ml-1 ddtl" id="d_<?= $berkas['idDokumen']; ?>"></i>
                                <i class="fas fa-trash btn btn-danger ml-1 drmv" id="r_<?= $berkas['idDokumen']; ?>"></i>
                            <?php else : ?>
                                &nbsp;
                            <?php endif; ?>
                        </td>
                    </tr>
                <?php endforeach; ?>
            </tbody>
        </table>
        <?php $this->view('template/pagination', $data); ?>
    </div>
</div>
<?php $this->view('template/bs4js'); ?>
<script src="<?= BASEURL . "js/dok-index.js"; ?>"></script>