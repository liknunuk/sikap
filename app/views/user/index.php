<div class="row boxTitle">
    <div class="col">
        <h4>Daftar Pengguna <?= webTitle; ?></h4>
    </div>
</div>
<div class="row mt-3">
    <div class="col-lg-10 form-inline">
        <select id="param" class="form-control mr-1">
            <option value="">Pilih filter pencarian</option>
            <option value="namaLengkap">Nama Lengkap</option>
            <option value="nomorTelephone">Nomor Telephone</option>
            <option value="nomorInduk">Nomor Induk</option>
        </select>
        <input type="text" id="keyword" class="form-control mr-1" placeholder="Kata Kunci Pencarian">
    </div>
    <div class="col-lg-2">
        <a href="<?= BASEURL . "User/form"; ?>" class="btn btn-success">Pengguna Baru</a>
    </div>
</div>
<div class="row mt-3">
    <div class="col wh90 table-responsive">
        <?php Alert::sankil(); ?>
        <table class="table table-sm">
            <thead>
                <tr>
                    <!-- idPengguna , namaLengkap , nomorInduk , nomorTelephone , username , userauth , accessLevel -->
                    <th>Nomor Urut</th>
                    <th>Nomor Induk</th>
                    <th>Nama Lengkap</th>
                    <th>Nomor Telephone</th>
                    <th>Akses Level</th>
                    <th><i class="fas fa-tasks btn btn-danger"></i></th>
                </tr>
            </thead>
            <tbody id="userData">
                <?php
                $nomor = ($data['pn'] - 1) * rows + 1;
                foreach ($data['users'] as $user) :
                    $al = $user['accessLevel'];
                ?>
                    <tr>
                        <td><?= $nomor++; ?></td>
                        <td><?= $user['nomorInduk']; ?></td>
                        <td><?= $user['namaLengkap']; ?></td>
                        <td><?= $user['nomorTelephone']; ?></td>
                        <td><?= $data['kamus'][$al]; ?></td>
                        <td>
                            <i class="mx-1 btn btn-info fas fa-edit uchg" id="e_<?= $user['idPengguna']; ?>"></i>
                            <i class="mx-1 btn btn-danger fas fa-trash urmv" id="r_<?= $user['idPengguna']; ?>"></i>
                        </td>
                    </tr>

                <?php endforeach; ?>
            </tbody>
        </table>
        <?php $this->view('template/pagination', $data); ?>
    </div>
</div>
<?php $this->view('template/bs4js'); ?>
<script src="<?= BASEURL . "js/user-index.js"; ?>"></script>