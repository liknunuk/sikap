<?php
function acakPassword()
{
    $frase1 = ['A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q'];
    $frase2 = ['z', 'y', 'x', 'v', 'u', 't', 's', 'r', 'q', 'p', 'o', 'n', 'm', 'l', 'k', 'j', 'i'];
    $frase3 = ['0', '1', '2', '3', '4', '5', '6', '7', '8', '9'];
    // password = AA0aAaa0
    // password = 01234567
    $password = "";
    for ($i = 0; $i < 8; $i++) {
        if ($i < 2) {
            $ac = acakAngka(16);
            $password .= $frase1[$ac];
        }
        if ($i == 2) {
            $ac = acakAngka(9);
            $password .= $frase3[$ac];
        }
        if ($i == 3) {
            $ac = acakAngka(16);
            $password .= $frase2[$ac];
        }
        if ($i == 4) {
            $ac = acakAngka(16);
            $password .= $frase1[$ac];
        }
        if ($i == 5 || $i == 6) {
            $ac = acakAngka(16);
            $password .= $frase2[$ac];
        }
        if ($i == 7) {
            $ac = acakAngka(9);
            $password .= $frase3[$ac];
        }
    }
    return $password;
}

function acakAngka($angka)
{
    return mt_rand(0, $angka);
}
?>
<div class="row boxTitle">
    <div class="col">
        <h4>Pengguna</h4>
    </div>
</div>
<div class="row mt-3">
    <div class="col-lg-6 mx-auto wh90">
        <form action="<?= BASEURL . "User/setUser"; ?>" method="post">
            <input type="hidden" name="modus" value="<?= $data['modus']; ?>">
            <input type="hidden" name="idPengguna" value="<?= $data['userId']; ?>">
            <div class="form-group row">
                <label for="nomorInduk" class="col-md-4">Nomor Induk</label>
                <div class="col-md-8">
                    <input type="text" name="nomorInduk" id="nomorInduk" class="form-control" value="<?php echo $data['modus'] == 'append' ? NULL : $data['user']['nomorInduk']; ?>">
                </div>
            </div>

            <div class="form-group row">
                <label for="namaLengkap" class="col-md-4">Nama Lengkap</label>
                <div class="col-md-8">
                    <input type="text" name="namaLengkap" id="namaLengkap" class="form-control" value="<?php echo $data['modus'] == 'append' ? NULL : $data['user']['namaLengkap']; ?>">
                </div>
            </div>

            <div class="form-group row">
                <label for="nomorTelephone" class="col-md-4">Nomor Telepon</label>
                <div class="col-md-8">
                    <input type="text" name="nomorTelephone" id="nomorTelephone" class="form-control" value="<?php echo $data['modus'] == 'append' ? NULL : $data['user']['nomorTelephone']; ?>">
                </div>
            </div>

            <?php if ($data['modus'] == 'append') : ?>

                <div class="form-group row">
                    <label for="username" class="col-md-4">Username</label>
                    <div class="col-md-8">
                        <input type="text" name="username" id="username" class="form-control" value="<?php echo $data['modus'] == 'append' ? NULL : $data['user']['username']; ?>">
                    </div>
                </div>

                <div class="form-group row">
                    <label for="userauth" class="col-md-4">Password</label>
                    <div class="col-md-8">
                        <input style="font-family:monospace; font-weight:bold;" type="text" name="userauth" id="userauth" class="form-control" value="<?php echo acakPassword(); ?>">
                    </div>
                </div>

            <?php endif; ?>

            <div class="form-group row">
                <label for="accessLevel" class="col-md-4">Hak Akses</label>
                <div class="col-md-8">
                    <select name="accessLevel" id="accessLevel" class="form-control">
                        <option value="">Pilih Level Akses</option>
                        <option value="1">Pengguna</option>
                        <option value="2">Petugas</option>
                        <option value="3">Administrator</option>
                    </select>
                </div>
            </div>
            <div class="form-group d-flex justify-content-center">
                <button type="submit" class="btn btn-primary">Simpan</button>
            </div>
        </form>
        <!-- idPengguna , namaLengkap , nomorInduk , nomorTelephone , username , userauth , accessLevel -->
    </div>
</div>