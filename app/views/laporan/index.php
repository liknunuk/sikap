<div class="row boxTitle">
    <div class="col">
        <h4>Laporan SIKAP</h4>
    </div>
</div>
<div class="row mt-3 wh90 bg-secondary mx-2">
    <!-- Statistik Peminjaman -->
    <div class="card col-md-4 border">
        <div class="card-header bg-dark text-light">
            STATISTIK PEMINJAMAN
        </div>
        <div class="card-body table-responsive">
            <table class="table table-bordered">
                <tbody>
                    <tr>
                        <th>Jumlah</th>
                        <td class='text-right'><?= $data['peminjaman']['jumlah']; ?></td>
                    </tr>
                    <tr>
                        <th>Berkas Dipinjam</th>
                        <td class='text-right'><?= $data['peminjaman']['berkas']; ?></td>
                    </tr>
                    <tr>
                        <th>Peminjam</th>
                        <td class='text-right'><?= $data['peminjaman']['peminjam']; ?></td>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>
    <!-- Statistik Dokumen -->
    <div class="card col-md-4 border">
        <div class="card-header bg-dark text-light">
            STATISTIK BERKAS
        </div>
        <div class="card-body table-responsive">
            <table class="table table-bordered">
                <tbody>
                    <tr>
                        <th>Total Berkas</th>
                        <td class='text-right'><?= $data['dokumen']['total']; ?></td>
                    </tr>
                    <tr>
                        <th>Berkas Dipinjam</th>
                        <td class='text-right'><?= $data['dokumen']['dipinjam']; ?></td>
                    </tr>
                    <?php foreach ($data['dokumen']['kategori'] as $kat) : ?>
                        <tr>
                            <th><?= $kat['kategori']; ?></th>
                            <td class='text-right'><?= $kat['jumlah']; ?></td>
                        </tr>
                    <?php endforeach; ?>
                </tbody>
            </table>
        </div>
    </div>
    <!-- Peminjam Terbanyak -->
    <div class="card col-md-4 border">
        <div class="card-header bg-dark text-light">
            PEMINJAM TERBANYAK
        </div>
        <div class="card-body table-responsive">
            <table class="table table-bordered">
                <tbody>

                    <?php foreach ($data['peminjam'] as $user) : ?>
                        <tr>
                            <th><?= $user['peminjam']; ?></th>
                            <td class='text-right'><?= $user['intensitas']; ?></td>
                        </tr>
                    <?php endforeach; ?>
                </tbody>
            </table>
        </div>
    </div>
</div>

<?php $this->view('template/bs4js'); ?>