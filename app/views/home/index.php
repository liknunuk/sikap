<div class="row wh90">
    <div class="col-lg-4 mx-auto">

        <div id="loginWrapper">
            <div class="boxTitle">
                Login <?= webTitle; ?>
            </div>
            <div id="loginLogo">
                <img src="https://upload.wikimedia.org/wikipedia/commons/5/51/Logo_BPN-KemenATR_%282017%29.png" alt="bpn">
            </div>
            <form action="<?= BASEURL . "Home/auth"; ?>" method="post">
                <div class="form-group">
                    <input type="text" name="username" id="" class="form-control rounded" placeholder="username">
                </div>
                <div class="form-group">
                    <input type="password" name="password" id="" class="form-control rounded" placeholder="password">
                </div>
                <div class="form-group d-flex justify-content-center">
                    <button type="submit" class="btn btn-warning">Login</button>
                </div>
            </form>
        </div>
        <?php Alert::sankil(); ?>
    </div>
</div>
<?php $this->view('template/bs4js'); ?>