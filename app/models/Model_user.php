<?php
class Model_user
{
    private $table = "pengguna";
    private $db;
    /*/ 
    namaLengkap=:namaLengkap , nomorInduk=:nomorInduk , username=:username , userauth=:userauth , accessLevel=:accessLevel , idPengguna=:idPengguna;
    /*/

    public function __construct()
    {
        $this->db = new Database();
    }


    // Basic CRUD OPERATION //

    // INSERTION
    public function tambah($data)
    {
        $data['userauth'] = $this->setUserauth($data['username'], $data['password']);
        $sql = "INSERT INTO $this->table SET namaLengkap=:namaLengkap , nomorInduk=:nomorInduk , nomorTelephone=:nomorTelephone , username=:username , userauth=:userauth , accessLevel=:accessLevel";
        $this->db->query($sql);
        $this->db->bind('namaLengkap', $data['namaLengkap']);
        $this->db->bind('nomorInduk', $data['nomorInduk']);
        $this->db->bind('nomorTelephone', $data['nomorTelephone']);
        $this->db->bind('username', $data['username']);
        $this->db->bind('userauth', $data['userauth']);
        $this->db->bind('accessLevel', $data['accessLevel']);
        $this->db->execute();
        return $this->db->rowCount();
    }

    // UPDATE
    public function ngubah($data)
    {
        $sql = "UPDATE $this->table SET namaLengkap=:namaLengkap , nomorInduk=:nomorInduk , nomorTelephone=:nomorTelephone ,  accessLevel=:accessLevel WHERE idPengguna=:idPengguna";
        $this->db->query($sql);
        $this->db->bind('namaLengkap', $data['namaLengkap']);
        $this->db->bind('nomorInduk', $data['nomorInduk']);
        $this->db->bind('nomorTelephone', $data['nomorTelephone']);
        $this->db->bind('accessLevel', $data['accessLevel']);
        $this->db->bind('idPengguna', $data['idPengguna']);
        $this->db->execute();
        return $this->db->rowCount();
    }

    // DELETE
    public function sampah($data)
    {
        $sql = "DELETE FROM $this->table WHERE idPengguna=:idPengguna";
        $this->db->query($sql);
        $this->db->bind('idPengguna', $data['idPengguna']);
        $this->db->execute();
        return $this->db->rowCount();
    }

    // DISPLAY MULTIPLE
    public function tampil($pn = 1)
    {
        $row = ($pn - 1) * rows;
        $sql = "SELECT * FROM $this->table ORDER BY namaLengkap LIMIT $row ," . rows;
        $this->db->query($sql);
        return $this->db->resultSet();
    }

    public function chUserauth($data)
    {
        $data['userauth'] = $this->setUserauth($data['username'], $data['password']);
        $sql = "UPDATE $this->table SET userauth=:userauth WHERE idPengguna=:idPengguna LIMIT 1";
        $this->db->query($sql);
        $this->db->bind('userauth', $data['userauth']);
        $this->db->bind('idPengguna', $data['idPengguna']);
        $this->db->execute();
        return $this->db->rowCount();
    }
    // DISPLAY SINGULAR
    public function detail($key)
    {
        $sql = "SELECT * FROM $this->table WHERE idPengguna=:key";
        $this->db->query($sql);
        $this->db->bind('key', $key);
        return $this->db->resultOne();
    }

    // Login
    public function authUser($data)
    {
        $data['userauth'] = $this->setUserAuth($data);
        $sql = "SELECT * FROM $this->table WHERE userauth=:userauth LIMIT 1";
        $this->db->query($sql);
        $this->db->bind('userauth', $data['userauth']);
        return $this->db->resultOne();
    }

    public function cari($data)
    {
        $sql = "SELECT * FROM $this->table WHERE {$data['param']} LIKE :keyword ORDER BY {$data['param']}";
        $this->db->query($sql);
        $this->db->bind('keyword', "%{$data['keyword']}%");
        return $this->db->resultSet();
    }

    private function setUserAuth($data)
    {
        return md5($data['username'] . "*99*" . $data['password']);
    }

    public function peminjamTerbanyak()
    {
        $tahun = '%-' . date('Y');
        $sql = "SELECT peminjam ,COUNT(peminjam) intensitas FROM viewPinjam WHERE tgPinjam LIKE :tahun GROUP BY peminjam ORDER BY COUNT(peminjam) desc LIMIT 10";
        $this->db->query($sql);
        $this->db->bind('tahun', $tahun);
        return $this->db->resultSet();
    }
}

// QUERY TEMPLATE
/*
$sql = "";
$this->db->query($sql);
$this->db->bind();
$this->db->execute();
return $this->db->resultSet();
return $this->db->resultOne();
*/
