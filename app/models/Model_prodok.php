<?php
class Model_prodok
{
    public function suratTanah()
    {
        $property = [
            'nomorHak' => 'Nomor Hak',
            'jenisHak' => 'Jenis Hak',
            'tahun' => 'Tahun Berkas',
            'pemegang' => 'Pemegang Hak',
            'desa' => 'Desa',
            'kecamatan' => 'Kecamatan'
        ];

        return $property;
    }

    public function bukuTanah()
    {
        $property = [
            'nomorHak' => 'Nomor Hak',
            'jenisHak' => 'Jenis Hak',
            'tahun' => 'Tahun Berkas',
            'pemegang' => 'Pemegang Hak',
            'desa' => 'Desa',
            'kecamatan' => 'Kecamatan'
        ];

        return $property;
    }

    public function suratUkur()
    {
        $property = [
            'nub' => 'Nomor Urut Berkas',
            'nib' => 'Nomor Identifikasi Bidang',
            'tahun' => 'Tahun Berkas',
            'pemohon' => 'Pemegang Hak',
            'desa' => 'Desa',
            'kecamatan' => 'Kecamatan'
        ];

        return $property;
    }

    public function gambarUkur()
    {
        $property = [
            'nomorPetaPendaftaran' => 'Nomor Peta Pendaftaran',
            'tahunBerkas' => 'Tahun Berkas',
            'pemohon' => 'Pemegang Hak',
            'pengukur' => 'Petugas Ukur',
            'tanggal' => 'Tanggal Pengukuran',
            'desa' => 'Desa',
            'kecamatan' => 'Kecamatan'
        ];

        return $property;
    }
}
