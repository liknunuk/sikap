<?php
class Model_peminjaman
{
    private $table = "peminjaman";
    private $tview = "viewPinjam";
    // private $detil = "dipinjam";
    private $db;
    /*/
    peminjam=:peminjam , petugas=:petugas , idDokumen=:idDokumen , tgPinjam=:tgPinjam , tgMbalik=:tgMbalik,trxPinjam=:trxPinjamrxPinjam
    /*/

    public function __construct()
    {
        $this->db = new Database();
    }


    // Basic CRUD OPERATION //

    // INSERTION
    public function tambah($data)
    {
        $sql = "INSERT INTO $this->table SET peminjam=:peminjam , petugas=:petugas, tgPinjam=:tgPinjam , trxPinjam=:trxPinjam";
        $this->db->query($sql);
        $this->db->bind('peminjam', $data['peminjam']);
        $this->db->bind('petugas', $data['petugas']);
        $this->db->bind('tgPinjam', $data['tgPinjam']);
        $this->db->bind('trxPinjam', $data['trxPinjam']);
        // $this->db->bind('idDokumen', $data['idDokumen']);
        $this->db->execute();
        return $this->db->rowCount();
    }

    // UPDATE
    public function ngubah($data)
    {
        $sql = "UPDATE $this->table SET peminjam=:peminjam , petugas=:petugas , idDokumen=:idDokumen , tgPinjam=:tgPinjam  WHERE  trxPinjam=:trxPinjam";
        $this->db->query($sql);
        $this->db->bind('peminjam', $data['peminjam']);
        $this->db->bind('petugas', $data['petugas']);
        $this->db->bind('idDokumen', $data['idDokumen']);
        $this->db->bind('tgPinjam', $data['tgPinjam']);
        $this->db->bind('trxPinjam', $data['trxPinjam']);
        $this->db->execute();
        return $this->db->rowCount();
    }

    // DELETE
    public function sampah($data)
    {
        $sql = "DELETE FROM $this->table WHERE trxPinjam=:trxPinjam ";
        $this->db->query($sql);
        $this->db->bind('trxPinjam', $data['trxPinjam']);
        $this->db->execute();
        return $this->db->rowCount();
    }

    // DISPLAY MULTIPLE
    public function tampil($pn = 1)
    {
        $row = ($pn - 1) * rows;
        $sql = "SELECT {$this->table}.trxPinjam , DATE_FORMAT({$this->table}.tgPinjam, '%d-%m-%Y') tgPinjam, DATE_FORMAT({$this->table}.tgMbalik, '%d-%m-%Y' ) tgMbalik, (SELECT pengguna.namaLengkap FROM pengguna WHERE idPengguna={$this->table}.peminjam) namaPeminjam , (SELECT pengguna.namaLengkap FROM pengguna WHERE pengguna.idPengguna={$this->table}.petugas) namaPetugas FROM peminjaman ORDER BY trxPinjam DESC LIMIT $row ," . rows;
        $this->db->query($sql);
        return $this->db->resultSet();
    }

    public function detilPinjaman($trxPinjam)
    {
        $sql = "SELECT * FROM $this->tview WHERE trxPinjam=:trxPinjam ORDER BY idDokumen";
    }

    // DISPLAY SINGULAR
    public function detail($key)
    {
        $sql = "SELECT *  FROM $this->tview WHERE trxPinjam=:key";
        $this->db->query($sql);
        $this->db->bind('key', $key);
        return $this->db->resultSet();
    }

    // CUSTOMIZED QUERY //
    public function pengembalian($data)
    {
        $data['tgMbalik'] = date('Y-m-d');
        $sql = "UPDATE $this->table SET tgMbalik=:tgMbalik WHERE trxPinjam=:trxPinjam";
        $this->db->query($sql);
        $this->db->bind('tgMbalik', $data['tgMbalik']);
        $this->db->bind('trxPinjam', $data['trxPinjam']);
        $this->db->execute();
        return $this->db->rowCount();
    }

    public function dokbalik($trxPinjam)
    {
        $sql = "SELECT idDokumen FROM $this->tview WHERE trxPinjam=:trxPinjam && tersedia='Dipinjam'";
        $this->db->query($sql);
        $this->db->bind('trxPinjam', $trxPinjam);
        return $this->db->resultSet();
    }

    public function nomorPinjam($today)
    {
        $today = dechex($today);
        // return $today;
        $sql = "SELECT MAX(trxPinjam) trxPinjam FROM peminjaman WHERE trxPinjam LIKE '{$today}%'";
        $this->db->query($sql);
        $result = $this->db->resultOne();
        if ($result['trxPinjam'] == NULL) {
            $trxPinjam = $today . "." . sprintf("%03d", 1);
        } else {
            $buntut = substr($result['trxPinjam'], -3);
            $trxPinjam = $today . "." . sprintf("%03d", ($buntut + 1));
        }

        return $trxPinjam;
    }

    public function dipinjam($docIds, $trxPinjam)
    {
        $rc = 0;
        foreach ($docIds as $idDokumen) {
            // echo $idDokumen . "<br/>";
            $sql = "INSERT INTO dipinjam SET trxPinjam=:trxPinjam , idDokumen=:idDokumen";
            $this->db->query($sql);
            $this->db->bind('trxPinjam', $trxPinjam);
            $this->db->bind('idDokumen', $idDokumen);
            $this->db->execute();
            $rc += $this->db->rowCount();
        }
        return $rc;
        // return 1;
    }

    // LAPORAN
    public function laporan($bulan = "")
    {
        $bulan = $bulan == "" ? date('Y-m') : $bulan;
        list($y, $b) = explode("-", $bulan);
        $bt = $bulan . '%';
        $bv = '%' . $b . "-" . $y;
        // echo "BT: " . $bt . " : BV: " . $bv;
        // exit();
        $data = [
            'jumlah' => $this->peminjamanBulanan($bt),
            'berkas' => $this->berkasDipinjamBulanan($bv),
            'peminjam' => $this->peminjamBulanan($bt)
        ];

        return $data;
    }

    private function peminjamanBulanan($bulan)
    {
        // bulan = '2021-12%'
        $sql = "SELECT COUNT(*) peminjaman FROM peminjaman WHERE tgPinjam LIKE :bulan ";
        $this->db->query($sql);
        $this->db->bind('bulan', $bulan);
        $result = $this->db->resultOne();
        return $result['peminjaman'];
    }

    private function berkasDipinjamBulanan($bulan)
    {
        // bulan = '%12-2021'
        $sql = "SELECT COUNT(*) berkas FROM viewPinjam WHERE tgPinjam LIKE :bulan ";
        $this->db->query($sql);
        $this->db->bind('bulan', $bulan);
        $result = $this->db->resultOne();
        return $result['berkas'];
    }

    private function peminjamBulanan($bulan)
    {
        // bulan = '2021-12%'
        $sql = "SELECT COUNT(peminjam) peminjam FROM peminjaman WHERE tgPinjam LIKE :bulan GROUP BY peminjam";
        $this->db->query($sql);
        $this->db->bind('bulan', $bulan);
        $result = $this->db->resultSet();
        return COUNT($result);
    }
}

// QUERY TEMPLATE
/*
$sql = "";
$this->db->query($sql);
$this->db->bind();
$this->db->execute();
return $this->db->resultSet();
return $this->db->resultOne();
*/
