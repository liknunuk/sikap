<?php
class Model_dokumen
{
    private $table = "dokumen";
    private $db;
    /*/ 
    nomorBerkas=:nomorBerkas , kategori=:kategori , lokasi=:lokasi , tersedia=:tersedia , property=:property , idDokumen=:idDokumen
    /*/

    public function __construct()
    {
        $this->db = new Database();
    }


    // Basic CRUD OPERATION //

    // INSERTION
    public function tambah($data)
    {
        $idDokumen = md5($data['nomorBerkas']);
        $sql = "INSERT INTO $this->table SET nomorBerkas=:nomorBerkas , kategori=:kategori , lokasi=:lokasi , tersedia=:tersedia , property=:property , idDokumen=:idDokumen";
        $this->db->query($sql);
        $this->db->bind('nomorBerkas', $data['nomorBerkas']);
        $this->db->bind('kategori', $data['kategori']);
        $this->db->bind('lokasi', $data['lokasi']);
        $this->db->bind('tersedia', 'Ada');
        $this->db->bind('property', $data['property']);
        $this->db->bind('idDokumen', $idDokumen);
        $this->db->execute();
        return $this->db->rowCount();
    }

    // UPDATE
    public function ngubah($data)
    {
        $idDokumen = md5($data['nomorBerkas']);

        $sql = "UPDATE $this->table SET kategori=:kategori , lokasi=:lokasi , tersedia=:tersedia , property=:property WHERE idDokumen=:idDokumen ";
        $this->db->query($sql);
        $this->db->bind('kategori', $data['kategori']);
        $this->db->bind('lokasi', $data['lokasi']);
        $this->db->bind('tersedia', 'Ada');
        $this->db->bind('property', $data['property']);
        $this->db->bind('idDokumen', $idDokumen);
        $this->db->execute();
        return $this->db->rowCount();
    }

    public function dipinjam($idDokumen)
    {
        $sql = "UPDATE $this->table SET tersedia='Dipinjam' WHERE idDokumen=:idDokumen LIMIT 1";
        $this->db->query($sql);
        $this->db->bind('idDokumen', $idDokumen);
        $this->db->execute();
        return $this->db->rowCount();
    }

    public function kembali($idDokumen)
    {
        $sql = "UPDATE $this->table SET tersedia='Ada' WHERE idDokumen=:idDokumen LIMIT 1";
        $this->db->query($sql);
        $this->db->bind('idDokumen', $idDokumen);
        $this->db->execute();
        return $this->db->rowCount();
    }

    // DELETE
    public function sampah($data)
    {
        $sql = "DELETE FROM $this->table WHERE idDokumen=:idDokumen ";
        $this->db->query($sql);
        $this->db->bind('idDokumen', $data['idDokumen']);
        $this->db->execute();
        return $this->db->rowCount();
    }

    // DISPLAY MULTIPLE
    public function tampil($pn = 1)
    {
        $row = ($pn - 1) * rows;
        $sql = "SELECT * FROM $this->table ORDER BY idDokumen LIMIT $row ," . rows;
        $this->db->query($sql);
        return $this->db->resultSet();
    }

    // DISPLAY SINGULAR
    public function detail($key)
    {
        $sql = "SELECT * FROM $this->table WHERE idDokumen=:key ";
        $this->db->query($sql);
        $this->db->bind('key', $key);
        return $this->db->resultOne();
    }

    // CUSTOMIZED QUERY //
    public function cari($data)
    {
        $param = $data['param'];
        $sql = "SELECT * FROM $this->table WHERE {$data['param']} LIKE :keyword";
        $this->db->query($sql);
        $this->db->bind('keyword', "%{$data['keyword']}%");
        return $this->db->resultSet();
    }

    public function cariProperty($data)
    {
        $param = $data['param'];
        $sql = "SELECT * FROM $this->table WHERE JSON_SEARCH(property,'all',:propval,NULL,'$.$param') IS NOT NULL";
        $this->db->query($sql);
        $this->db->bind('propval', "%{$data['keyword']}%");
        return $this->db->resultSet();
    }

    public function setTersedia($data)
    {
        $sql = "UPDATE $this->table SET tersedia=:tersedia WHERE idDokumen=:idDokumen";
        $this->db->query($sql);
        $this->db->bind('tersedia', $data['tersedia']);
        $this->db->bind('idDokumen', $data['idDokumen']);
        $this->db->execute();
        return $this->db->rowCount();
    }

    // private function tanggalMbalik(string $idDokumen)
    // {
    //     $now = date('Y-m-d');
    //     $sql = "UPDATE peminjaman SET tgMbalik = '{$now}' WHERE idDokumen=:idDokumen LIMIT 1";
    //     $this->db->query($sql);
    //     $this->db->bind('idDokumen', $idDokumen);
    //     $this->db->execute();
    //     return $this->db->rowCount();
    // }

    public function dokDipinjam()
    {
        $sql = "SELECT * FROM $this->table WHERE tersedia='dipinjam'";
        $this->db->query($sql);
        return $this->db->resultSet();
    }

    public function statistik()
    {
        $totalDokumen = $this->totalDokumen();
        $totalDipinjam = $this->totalDipinjam();
        $jumlahKategory = $this->dokbykategori();
        return array('total' => $totalDokumen, 'dipinjam' => $totalDipinjam, 'kategori' => $jumlahKategory);
    }

    private function totalDokumen()
    {
        $sql = " SELECT COUNT(*) dokumen FROM $this->table ";
        $this->db->query($sql);
        $result = $this->db->resultOne();
        return $result['dokumen'];
    }

    private function totalDipinjam()
    {
        $sql = " SELECT COUNT(*) dipinjam FROM $this->table WHERE tersedia='Dipinjam' ";
        $this->db->query($sql);
        $result = $this->db->resultOne();
        return $result['dipinjam'];
    }

    private function dokbykategori()
    {
        $sql = "SELECT kategori , COUNT(kategori) jumlah FROM $this->table GROUP BY kategori ORDER BY kategori";
        $this->db->query($sql);
        return $this->db->resultSet();
    }
}

// QUERY TEMPLATE
/*
$sql = "";
$this->db->query($sql);
$this->db->bind();
$this->db->execute();
return $this->db->resultSet();
return $this->db->resultOne();
*/
