<?php
// sesuaikan nama kelas, tetap extends ke Controller
class User extends Controller
{
  // method default
  public function index($pn = 1)
  {
    // -- Columns: idPengguna , namaLengkap , nomorInduk , nomorTelephone , username , userauth , accessLevel
    $data = [
      'title' => 'Pengguna',
      'users' => $this->model('Model_user')->tampil($pn),
      'kamus' => $this->model('Model_kamus')->userLevel(),
      'pn' => $pn,
      'pageurl' => BASEURL . "User/",
      'al' => 3
    ];

    $this->view('template/header', $data);
    $this->view('template/smallBrand', $data);
    $this->view('template/tmpMenu', $data);
    $this->view('user/index', $data);
    $this->view('template/spacer');
    $this->view('template/footer');
  }

  public function userFilter()
  {
    $data = [
      'users' => $this->model('Model_user')->cari($_POST),
      'kamus' => $this->model('Model_kamus')->userLevel()
    ];

    $users = [];
    // idPengguna , namaLengkap , nomorInduk , nomorTelephone , username , userauth , accessLevel
    foreach ($data['users'] as $user) {
      $al = $user['accessLevel'];
      array_push(
        $users,
        [
          'idPengguna' => $user['idPengguna'],
          'namaLengkap' => $user['namaLengkap'],
          'nomorInduk' => $user['nomorInduk'],
          'nomorTelephone' => $user['nomorTelephone'],
          'accessLevel' => $data['kamus'][$al]
        ]
      );
    }
    echo json_encode($users, JSON_PRETTY_PRINT);
  }

  public function form($userId = false)
  {
    $user = $userId == false ? NULL : $this->Model('Model_user')->detail($userId);
    $mods = $userId == false ?  'append' : 'change';
    $data = [
      'user' => $user,
      'userId' => $userId,
      'title' => 'Pengguna',
      'modus' => $mods,
      'al' => 3
    ];

    $this->view('template/header', $data);
    $this->view('template/smallBrand', $data);
    $this->view('template/tmpMenu', $data);
    $this->view('user/userForm', $data);
    $this->view('template/spacer');
    $this->view('template/footer');
  }

  public function setUser()
  {
    if ($_POST['modus'] == 'append') {
      if ($this->model('Model_user')->tambah($_POST) > 0) {
        Alert::setAlert('berhasil ditambahkan', 'Data pengguna', 'success');
      } else {
        Alert::setAlert('gagal ditambahkan', 'Data pengguna', 'danger');
      }
    } else {
      if ($this->model('Model_user')->ngubah($_POST) > 0) {
        Alert::setAlert('berhasil ditambahkan', 'Data pengguna', 'success');
      } else {
        Alert::setAlert('gagal ditambahkan', 'Data pengguna', 'danger');
      }
    }
    header('Location:' . BASEURL . 'User');
  }

  public function fired()
  {
    echo $this->model('Model_user')->sampah($_POST) > 0 ? "1" : "0";
  }

  public function nameSeek($nama)
  {
    $data = ['param' => 'namaLengkap', 'keyword' => $nama];
    $users = $this->model('Model_user')->cari($data);
    echo json_encode($users, JSON_PRETTY_PRINT);
  }
}
