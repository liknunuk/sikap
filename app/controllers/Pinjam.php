<?php
// sesuaikan nama kelas, tetap extends ke Controller
class Pinjam extends Controller
{
  // method default
  public function index($pn = 1)
  {
    // trxPinjam , peminjam, petugas, idDokumen, tgPinjam
    $data = [
      'peminjaman' => $this->model('Model_peminjaman')->tampil($pn),
      'title' => 'peminjaman'
    ];
    $this->view('template/header', $data);
    $this->view('template/smallBrand', $data);
    $this->view('template/tmpMenu', $data);
    $this->view('pinjam/index', $data);
    $this->view('template/spacer');
    $this->view('template/footer');
  }

  public function form()
  {
    $today = date('ymd');
    $data = [
      'title' => 'Peminjaman',
      'trxId' => $this->model('Model_peminjaman')->nomorPinjam($today)
    ];

    $this->view('template/header', $data);
    $this->view('template/smallBrand', $data);
    $this->view('template/tmpMenu', $data);
    $this->view('pinjam/pjmForm', $data);
    $this->view('template/spacer');
    $this->view('template/footer');
  }

  public function cam()
  {
    $this->view('pinjam/instacan1');
  }

  public function set()
  {

    $cleanedDocId = str_replace(" ", "", $_POST['scanDocId']);
    $cleanedDocId = str_replace("\n", "", $cleanedDocId);
    $cleanedDocId = str_replace("\r", "", $cleanedDocId);
    $cleanedDocId = rtrim($cleanedDocId, ",");
    $docIds = explode(",", $cleanedDocId);
    // $docIds = explode(",", trim($_POST['scanDocId']));
    list($peminjam, $nama) = explode("-", $_POST['peminjam']);
    // var_dump($docIds);
    // exit();

    $data = [
      'idDokumen' => $docIds,
      'petugas' => $_POST['petugas'],
      'trxPinjam' => $_POST['trxPinjam'],
      'tgPinjam' => $_POST['tgPinjam'],
      'peminjam' => $peminjam
    ];
    // exit();

    if ($this->model('Model_peminjaman')->tambah($data) > 0) {
      if ($this->model('Model_peminjaman')->dipinjam($docIds, $_POST['trxPinjam']) > 0) {
        Alert::setAlert('berhasil disimpan', 'Data peminjaman', 'success');
      } else {
        Alert::setAlert('gagal update', 'Daftar buku dipnjama', 'warning');
      }
    } else {
      Alert::setAlert('gagal disimpan', 'Data peminjaman', 'danger');
    }
    header("Location:" . BASEURL . "Pinjam");
  }

  public function detil($trxPinjam)
  {
    $data = [
      'title' => 'Info pinjam',
      'pinjam' => $this->model('Model_peminjaman')->detail($trxPinjam),
      'trxPinjam' => $trxPinjam
    ];
    $this->view('template/header', $data);
    $this->view('template/smallBrand', $data);
    $this->view('template/tmpMenu', $data);
    $this->view('pinjam/detil', $data);
    $this->view('template/spacer');
    $this->view('template/footer');
  }

  public function balikberkas()
  {
    echo $this->model('Model_dokumen')->kembali($_POST['idDokumen']) > 0 ? "1" : '0';
  }

  public function balikpinjam()
  {
    $gagal = 0;
    $berkasdipinjam = $this->Model('Model_peminjaman')->dokbalik($_POST['trxPinjam']);

    foreach ($berkasdipinjam as $berkas) {
      $data = ['tersedia' => 'Ada', 'idDokumen' => $berkas['idDokumen']];
      if ($this->model('Model_dokumen')->setTersedia($data) > 0) {
        echo "{$berkas['idDokumen']} berhasil \n";
        $gagal += 0;
      } else {
        echo "{$berkas['idDokumen']} gagal \n";
        $gagal += 1;
      }
    }

    if ($gagal == 0) {
      $success = $this->model('Model_peminjaman')->pengembalian($_POST) > 0 ? "1" : '0';
      echo "tanggal Kembali: " . $success;
    } else {
      echo "tanggal Kembali: 0";
    }
  }
}
