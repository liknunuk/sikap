<?php
// sesuaikan nama kelas, tetap extends ke Controller
class Home extends Controller
{
  // method default
  public function index($al = 1)
  {
    $data['title'] = "login";
    $data['al'] = $al;
    $this->view('template/header', $data);
    // $this->view('template/bigBrand');
    $this->view('template/smallBrand');
    // $this->view('template/tmpMenu', $data);
    $this->view('home/index');
    $this->view('template/footer');
  }

  public function auth()
  {
    $user = $this->model('Model_user')->authUser($_POST);
    if (empty($user)) {
      Alert::setAlert('tidak ditemukan', 'User', 'danger');
      header("Location:" . BASEURL);
    } else {
      // print_r($user);
      /*/ 
      [idPengguna] => 102 
      [namaLengkap] => Admin 
      [nomorInduk] => 00003 
      [nomorTelephone] => 0286591066 
      [username] => admin 
      [userauth] => 533b643be4ba1ed9fbb8ab7af1a7ef76 
      [accessLevel] => 3
      /*/
      $_SESSION['idPengguna'] = $user['idPengguna'];
      $_SESSION['namaLengkap'] = $user['namaLengkap'];
      $_SESSION['accessLevel'] = $user['accessLevel'];
      switch ($user['accessLevel']) {
        case '1':
          $location = BASEURL . "Pustaka";
          break;
        case '2':
          $location = BASEURL . "Pinjam";
          break;
        case '3':
          $location = BASEURL . "User";
          break;
      }
      header("Location:" . $location);
    }
  }

  public function klilan()
  {
    session_unset();
    session_destroy();
    header("Location:" . BASEURL);
  }

  public function test()
  {
    $cari = ['param' => 'pemilik', 'keyword' => 'eni'];
    $data['dokumen'] = $this->model('Model_dokumen')->cariProperty($cari);
    echo "<pre>";
    foreach ($data['dokumen'] as $dok) {
      echo "
      Nomor Dokumen      : " . $dok['idDokumen'] . ". \n
      Kategori Dokumen   : " . $dok['kategori'] . " \n
      Tempat Penyimpanan : " . $dok['lokasi'] . " \n
      Ketersediaan       : " . $dok['tersedia'] . "\n      
      ";
      $info = json_decode($dok['property']);
      foreach ($info as $par => $val) {
        echo "$par \t : $val \n";
      }
    }
    echo "</pre>";
  }

  public function pagetest()
  {
    $data = ['title' => 'tes halaman'];
    $this->view('template/header', $data);
    $this->view('template/pagetest', $data);
    $this->view('template/footer', $data);
  }
}
