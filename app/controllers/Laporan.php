<?php
// sesuaikan nama kelas, tetap extends ke Controller
class Laporan extends Controller
{
    // method default
    public function index($bulan = "")
    {
        $data = [
            'title' => 'Laporan Peminjaman',
            'pageurl' => BASEURL . "Pustaka/",
            'peminjaman' => $this->model('Model_peminjaman')->laporan(),
            'dokumen' => $this->model('Model_dokumen')->statistik(),
            'peminjam' => $this->model('Model_user')->peminjamTerbanyak()
        ];

        $this->view('template/header', $data);
        $this->view('template/smallBrand', $data);
        $this->view('template/tmpMenu', $data);
        $this->view('laporan/index', $data);
        $this->view('template/spacer');
        $this->view('template/footer');
    }
}
