<?php
// sesuaikan nama kelas, tetap extends ke Controller
class Pustaka extends Controller
{
  // method default
  public function index($pn = 1)
  {
    $data = [
      'pn' => $pn,
      'title' => 'Arsip Pertanahan',
      'berkas' => $this->model('Model_dokumen')->tampil($pn),
      'pn' => $pn,
      'pageurl' => BASEURL . "Pustaka/",
    ];

    $this->view('template/header', $data);
    $this->view('template/smallBrand', $data);
    $this->view('template/tmpMenu', $data);
    $this->view('dokumen/index', $data);
    $this->view('template/spacer');
    $this->view('template/footer');
  }

  public function form($doctype, $docId = false)
  {
    switch ($doctype) {
      case 'bt':
        $properties = $this->model('Model_prodok')->suratTanah();
        $doctitle = 'Buku Tanah';
        break;
      case 'st':
        $properties = $this->model('Model_prodok')->suratTanah();
        $doctitle = 'Surat Tanah';
        break;
      case 'su':
        $properties = $this->model('Model_prodok')->suratUkur();
        $doctitle = 'Surat Ukur';
        break;
      case 'gu':
        $properties = $this->model('Model_prodok')->gambarUkur();
        $doctitle = 'Gambar Ukur';
        break;
      default:
        $properties = $this->model('Model_prodok')->suratTanah();
        $doctitle = 'Buku Tanah';
    }
    $data = [
      'idDokumen' => $docId,
      'title' => 'Arsip Pertanahan',
      'berkas' => $this->model('Model_dokumen')->detail($docId),
      'properties' => $properties,
      'doctitle' => $doctitle,
      'modus' => $docId == false ? 'append' : 'change'
    ];

    $this->view('template/header', $data);
    $this->view('template/smallBrand', $data);
    $this->view('template/tmpMenu', $data);
    $this->view('dokumen/dokForm', $data);
    $this->view('template/spacer');
    $this->view('template/footer');
  }

  public function setArsip()
  {
    $json = json_encode($_POST['property']);
    $_POST['property'] = $json;
    if ($_POST['modus'] == 'append') {
      if ($this->model('Model_dokumen')->tambah($_POST) > 0) {
        Alert::setAlert('berhasil ditambahkan', 'Arsip pertanahan', 'success');
      } else {
        Alert::setAlert('gagal ditambahkan', 'Arsip pertanahan', 'danger');
      }
      $_SESSION['docId'] = md5($_POST['nomorBerkas']);
      header("Location:" . BASEURL . "Pustaka/barcoding");
    } else {
      if ($this->model('Model_dokumen')->ngubah($_POST) > 0) {
        Alert::setAlert('berhasil dimutakhirkan', 'Arsip pertanahan', 'success');
      } else {
        Alert::setAlert('gagal dimutakhirkan', 'Arsip pertanahan', 'danger');
      }
      header("Location:" . BASEURL . "Pustaka");
    }
  }

  public function detail($docId)
  {
    $data = [
      'title' => 'detail berkas',
      'berkas' => $this->model('Model_dokumen')->detail($docId)
    ];

    switch ($data['berkas']['kategori']) {
      case 'Surat Tanah':
        $data['properties'] = $this->model('Model_prodok')->suratTanah();
        break;
      case 'Surat Ukur':
        $data['properties'] = $this->model('Model_prodok')->suratUkur();
        break;
      case 'Gambar Ukur':
        $data['properties'] = $this->model('Model_prodok')->gambarUkur();
        break;
    }

    $this->view('template/header', $data);
    $this->view('template/smallBrand', $data);
    $this->view('template/tmpMenu', $data);
    $this->view('dokumen/doctail', $data);
    $this->view('template/spacer');
    $this->view('template/footer');
  }

  public function doctype($docId)
  {
    $data = ['param' => 'idDokumen', 'keyword' => $docId];
    $berkas = $this->model('Model_dokumen')->cari($data);

    switch ($berkas[0]['kategori']) {
      case 'Surat Tanah':
        $docType = 'st';
        break;
      case 'Surat Ukur':
        $docType = 'su';
        break;
      case 'Gambar Ukur':
        $docType = 'gu';
        break;
    }

    header("Location:" . BASEURL . "Pustaka/form/{$docType}/{$docId}");
  }

  public function pemusnahan()
  {
    echo $this->model('Model_dokumen')->sampah($_POST) > 0 ? "1" : "0";
  }

  public function cari()
  {
    $data = $this->model('Model_dokumen')->cari($_POST);
    echo json_encode($data, JSON_PRETTY_PRINT);
  }

  public function barcoding()
  {
    $data = [
      'title' => 'QR Code',
      'docId' => $_SESSION['docId']
    ];
    // 'docId' => '01044cfd18dd5c0cde26733c11a3b542'

    $this->view('dokumen/barcoding', $data);
  }
}
